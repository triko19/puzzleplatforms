// Fill out your copyright notice in the Description page of Project Settings.

#include "DoorTrigger.h"
#include "Components/BoxComponent.h"

// Sets default values
ADoorTrigger::ADoorTrigger()
{ 	
	PrimaryActorTick.bCanEverTick = true;

	TriggerVolume = CreateDefaultSubobject<UBoxComponent>(FName("TriggerVolume"));

	if (!ensure(TriggerVolume != nullptr))
	{
		UE_LOG(LogTemp, Error, TEXT("[CUSTOM]: TriggerVolume is nullptr"));
		return;
	}

	RootComponent = TriggerVolume;

	TriggerVolume->OnComponentBeginOverlap.AddDynamic(this, &ADoorTrigger::OnOverlapBegin);
	TriggerVolume->OnComponentEndOverlap.AddDynamic(this, &ADoorTrigger::OnOverlapEnd);
}

// Called when the game starts or when spawned
void ADoorTrigger::BeginPlay()
{
	Super::BeginPlay();	
		
	if (ActorHasTag("gravity"))
	{
		UE_LOG(LogTemp, Warning, TEXT("[%s] I got tag"), *GetName());
	}
}

// Called every frame
void ADoorTrigger::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ADoorTrigger::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{		
	UE_LOG(LogTemp, Warning, TEXT("Activeted"));
	for (ASpecialDoor* Door : DoorsToTrigger)
	{
		Door->AddActiveTrigger();
	}		

	GetOverlappingActors(OverlappingActor);	
	for (AActor* Actor : OverlappingActor)
	{					
		Actor->FindComponentByClass<UCharacterMovementComponent>()->GravityScale = .5f;		
		UE_LOG(LogTemp, Warning, TEXT("Actor name: %s"), *Actor->GetName());		
	}
}

void ADoorTrigger::OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	UE_LOG(LogTemp, Warning, TEXT("Deactiveted"));
	for (ASpecialDoor* Door : DoorsToTrigger)
	{
		Door->RemoveActiveTrigger();
	}
}
