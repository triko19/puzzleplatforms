// Fill out your copyright notice in the Description page of Project Settings.

#include "SetInitialTriggers.h"

#include "Components/BoxComponent.h"

#include "Engine/Engine.h"
#include "Engine/World.h"
#include "EngineUtils.h"
#include "Engine/StaticMeshActor.h"

#include "GameFramework/PlayerController.h"
#include "GameFramework/GameMode.h"
#include "GameFramework/Actor.h"

#include "SpecialDoor.h"

// Sets default values
ASetInitialTriggers::ASetInitialTriggers()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	TriggerVolume = CreateDefaultSubobject<UBoxComponent>(FName("TriggerVolume"));
	if (!ensure(TriggerVolume != nullptr))
	{
		UE_LOG(LogTemp, Error, TEXT("[CUSTOM]: TriggerVolume is nullptr"));
		return;
	}

	RootComponent = TriggerVolume;
	TriggerVolume->InitBoxExtent(FVector(350.f, 10.f, 10.f));

	TriggerVolume->OnComponentBeginOverlap.AddDynamic(this, &ASetInitialTriggers::OnOverlapBegin);
}

// Called when the game starts or when spawned
void ASetInitialTriggers::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASetInitialTriggers::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASetInitialTriggers::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{	
	if ((OtherActor != nullptr) && (OtherActor != this) && (OtherComp != nullptr))
	{		
		if (HasAuthority())
		{
			uint8 NPlayers = GetWorld()->GetAuthGameMode()->GetNumPlayers();
			UE_LOG(LogTemp, Warning, TEXT("PLAYER %d"), NPlayers);
			if (NPlayers > 2) NPlayers = 2;
			else NPlayers = 1;

			for (TObjectIterator<ASpecialDoor> Itr; Itr; ++Itr)
			{
				ASpecialDoor* Door = *Itr;
				Door->SetTriggersNeeded(NPlayers);
			}

			this->Destroy();
		}
	}
}