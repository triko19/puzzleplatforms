// Fill out your copyright notice in the Description page of Project Settings.

#include "GravityTrigger.h"
#include "Components/BoxComponent.h"

// Sets default values
AGravityTrigger::AGravityTrigger()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	TriggerVolume = CreateDefaultSubobject<UBoxComponent>(FName("TriggerVolume"));

	if (!ensure(TriggerVolume != nullptr))
	{
		UE_LOG(LogTemp, Error, TEXT("[CUSTOM]: TriggerVolume is nullptr"));
		return;
	}

	RootComponent = TriggerVolume;

	TriggerVolume->OnComponentBeginOverlap.AddDynamic(this, &AGravityTrigger::OnOverlapBegin);
	TriggerVolume->OnComponentEndOverlap.AddDynamic(this, &AGravityTrigger::OnOverlapEnd);
}

// Called when the game starts or when spawned
void AGravityTrigger::BeginPlay()
{
	Super::BeginPlay();
	
	if (!ensure(GravityToTrigger != nullptr)) return;
}

// Called every frame
void AGravityTrigger::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AGravityTrigger::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	UE_LOG(LogTemp, Warning, TEXT("Activeted"));
	if (!ensure(GravityToTrigger != nullptr)) return;
	GravityToTrigger->AddActiveTrigger();	

	/*GetOverlappingActors(OverlappingActor);
	for (AActor* Actor : OverlappingActor)
	{
		Actor->FindComponentByClass<UCharacterMovementComponent>()->GravityScale = .5f;
		UE_LOG(LogTemp, Warning, TEXT("Actor name: %s"), *Actor->GetName());
	}*/
}

void AGravityTrigger::OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	UE_LOG(LogTemp, Warning, TEXT("Deactiveted"));
	if (!ensure(GravityToTrigger != nullptr)) return;
	GravityToTrigger->RemoveActiveTrigger();
}
