// Fill out your copyright notice in the Description page of Project Settings.

#include "OpenDoor.h"
#include "Kismet/KismetMathLibrary.h"

#include "Components/BoxComponent.h"

// Sets default values for this component's properties
AOpenDoor::AOpenDoor()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryActorTick.bCanEverTick = true;

	bOpen = false;

	TriggerVolume = CreateDefaultSubobject<UBoxComponent>(FName("TriggerVolume"));
	if (!ensure(TriggerVolume != nullptr))
	{
		UE_LOG(LogTemp, Error, TEXT("[CUSTOM]: TriggerVolume is nullptr"));
		return;
	}

	RootComponent = TriggerVolume;

	TriggerVolume->OnComponentBeginOverlap.AddDynamic(this, &AOpenDoor::OnOverlapBegin);
	TriggerVolume->OnComponentEndOverlap.AddDynamic(this, &AOpenDoor::OnOverlapEnd);
	
	Door = CreateDefaultSubobject<UStaticMeshComponent>(FName("Door"));
	Door->SetRelativeLocation(FVector(0.f, 50.f, -50.f));
	Door->SetupAttachment(RootComponent);	
}


// Called when the game starts
void AOpenDoor::BeginPlay()
{
	Super::BeginPlay();
}


// Called every frame
void AOpenDoor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	DoorRotation = Door->RelativeRotation;

	if (bOpen)
	{
		Door->SetRelativeRotation(FMath::Lerp(FQuat(DoorRotation), FQuat(FRotator(0.f, RotateValue, 0.f)), 0.05));
	}
	else
	{
		Door->SetRelativeRotation(FMath::Lerp(FQuat(DoorRotation), FQuat(FRotator(0.f, 0.f, 0.f)), 0.05));
	}
}

void AOpenDoor::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	UE_LOG(LogTemp, Warning, TEXT("OverlapIn"));
	if ((OtherActor != nullptr) && (OtherActor != this) && (OtherComp != nullptr))
	{
		FVector PawnLocation = OtherActor->GetActorLocation();
		FVector Direction = GetActorLocation() - PawnLocation;
		Direction = UKismetMathLibrary::LessLess_VectorRotator(Direction, GetActorRotation());

		if (Direction.X > 0.f)
		{
			RotateValue = 140.f;
		}
		else
		{
			RotateValue = -140.f;
		}

		bOpen = true;
	}
}

void AOpenDoor::OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	UE_LOG(LogTemp, Warning, TEXT("OverlapOut"));
	if ((OtherActor != nullptr) && (OtherActor != this) && (OtherComp != nullptr))
	{
		bOpen = false;
	}
}

