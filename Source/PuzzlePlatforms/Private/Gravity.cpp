// Fill out your copyright notice in the Description page of Project Settings.

#include "Gravity.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/Character.h"
#include "Engine/StaticMeshActor.h"
#include "Engine/World.h"
#include "EngineUtils.h"

//#include "Runtime/Engine/Public/EngineUtils.h"

// Sets default values
AGravity::AGravity()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AGravity::BeginPlay()
{
	Super::BeginPlay();
		
	/*for (TObjectIterator<ACharacter> Itr; Itr; ++Itr)
	{		
		float v = Itr->FindComponentByClass<UCharacterMovementComponent>()->GravityScale;
		// Same as with the Object Iterator, access the subclass instance with the * or -> operators.		
		UE_LOG(LogTemp, Error, TEXT("Actor Name: [%s] and his gravity [%f]"), *Itr->GetName(), v);		
	}*/	
}

// Called every frame
void AGravity::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (ActiveTriggers >= TriggersNeeded)
	{
		//UE_LOG(LogTemp, Error, TEXT("Gravity Trigger Active"));
		if (HasAuthority()) //true -> server; false -> client
		{		
			//UE_LOG(LogTemp, Warning, TEXT("Has Auth"));
			for (TObjectIterator<ACharacter> Itr; Itr; ++Itr)
			{
				Itr->FindComponentByClass<UCharacterMovementComponent>()->GravityScale = GravityValue;
				//UE_LOG(LogTemp, Warning, TEXT("Gravity Scale [%f]"), Itr->FindComponentByClass<UCharacterMovementComponent>()->GravityScale);
				// Same as with the Object Iterator, access the subclass instance with the * or -> operators.		
				//UE_LOG(LogTemp, Error, TEXT("Actor Name: [%s] and his gravity [%f]"), *Itr->GetName(), v);
			}
		}
	}
	else
	{
		//UE_LOG(LogTemp, Warning, TEXT("Active Triggers 0"));
		if (HasAuthority()) //true -> server; false -> client
		{
			for (TObjectIterator<ACharacter> Itr; Itr; ++Itr)
			{
				Itr->FindComponentByClass<UCharacterMovementComponent>()->GravityScale = GravityValue;
			}
		}
	}
}

void AGravity::AddActiveTrigger()
{
	ActiveTriggers++;	
}

void AGravity::RemoveActiveTrigger()
{
	if (ActiveTriggers > 0) { ActiveTriggers--; }	
}

