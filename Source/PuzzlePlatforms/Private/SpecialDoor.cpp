// Fill out your copyright notice in the Description page of Project Settings.

#include "SpecialDoor.h"
#include "Engine/World.h"
#include "EngineUtils.h"
#include "GameFramework/Character.h"
#include "GameFramework/GameMode.h"

#include "GameFramework/Actor.h"

ASpecialDoor::ASpecialDoor()
{
	PrimaryActorTick.bCanEverTick = true;
}

void ASpecialDoor::BeginPlay()
{
	Super::BeginPlay();
	
	//Triggers needed set in SetInitialTrigger class

	InitialRotation = GetActorRotation();	

	if (HasAuthority()) // must be executed by the server
	{
		SetReplicates(true);
		SetReplicateMovement(true);		
	}
}

void ASpecialDoor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);		

	if (ActiveTriggers >= TriggersNeeded)
	{		
		if (HasAuthority())
		{		
			SetActorRotation(InitialRotation + FRotator(0.f, AngleValue, 0.f));
		}
	}
	else
	{				
		if (HasAuthority())
		{			
			SetActorRotation(InitialRotation + FRotator(0.f, 0.f, 0.f));
		}
	}
}

void ASpecialDoor::AddActiveTrigger()
{	
	ActiveTriggers++;	
}

void ASpecialDoor::RemoveActiveTrigger()
{
	if (ActiveTriggers > 0) { ActiveTriggers--; }
}

void ASpecialDoor::SetTriggersNeeded(int NumOfTriggers)
{
	TriggersNeeded = NumOfTriggers;
}