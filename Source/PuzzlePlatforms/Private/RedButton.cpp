// Fill out your copyright notice in the Description page of Project Settings.

#include "RedButton.h"
#include "Components/BoxComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/Character.h"
#include "Gravity.h"

// Sets default values
ARedButton::ARedButton()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	
}

// Called when the game starts or when spawned
void ARedButton::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ARedButton::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}



