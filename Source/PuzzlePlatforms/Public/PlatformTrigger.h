// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"


#include "PlatformTrigger.generated.h"

/*
USTRUCT()
struct FTrickyCollision
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere)
	AMovingPlatform* Platform;

	UPROPERTY(EditAnywhere)
	bool bEnableCollision = true;

	void DisableCollision()
	{
		Platform->GetStaticMeshComponent()->SetCollisionProfileName("NoCollision");		
	}
	void EnableCollision()
	{
		Platform->GetStaticMeshComponent()->SetCollisionProfileName("Default");
	}
};
*/

UCLASS()
class PUZZLEPLATFORMS_API APlatformTrigger : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APlatformTrigger();
	

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;


private:	

	UPROPERTY(VisibleAnywhere)
	class UBoxComponent* TriggerVolume;

	UPROPERTY(EditAnywhere)
	TArray<class AMovingPlatform*> PlatformsToTrigger;
	
	UPROPERTY(EditAnywhere)
	TArray<class AMovingPlatform*> TrickyPlatforms;

	/*UPROPERTY(EditAnywhere)
	TArray<FTrickyCollision> TrickyPlatforms;*/

	UFUNCTION()
	void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	void OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

};