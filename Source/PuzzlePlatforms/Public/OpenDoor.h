// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Engine/TriggerVolume.h"
#include "GameFramework/Controller.h"

#include "OpenDoor.generated.h"


UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class PUZZLEPLATFORMS_API AOpenDoor : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	AOpenDoor();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;	

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// The owning door
	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* Door = nullptr;

	UPROPERTY(VisibleAnywhere)
	class UBoxComponent* TriggerVolume;

	UFUNCTION()
	void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	void OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	bool bOpen;
	float RotateValue;
	FRotator DoorRotation;
};
