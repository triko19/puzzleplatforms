// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"


#include "Gravity.generated.h"

UCLASS()
class PUZZLEPLATFORMS_API AGravity : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AGravity();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void AddActiveTrigger();
	void RemoveActiveTrigger();

	UPROPERTY(BlueprintReadWrite)	
	float GravityValue = 1.9f;

private:
	
	UPROPERTY(EditAnywhere)
	int TriggersNeeded = 0;	

	TArray<AActor*> OverlappingActor;
	TArray<AActor*> MyActors;
	int ActiveTriggers = 0;
};
