// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/StaticMeshActor.h"
#include "PuzzlePlatformsGameInstance.h"

#include "SpecialDoor.generated.h"

/**
 * 
 */
UCLASS()
class PUZZLEPLATFORMS_API ASpecialDoor : public AStaticMeshActor
{
	GENERATED_BODY()
	
public:
	ASpecialDoor();

	virtual void BeginPlay() override;

	virtual void Tick(float DeltaTime) override;

	void AddActiveTrigger();
	void RemoveActiveTrigger();
	void SetTriggersNeeded(int);

private:
	
	UPROPERTY(EditAnywhere)
	int TriggersNeeded = 0;
	
	UPROPERTY(EditAnywhere)
	float AngleValue = 0.f;
	
	int ActiveTriggers = 0;
	int NPlayers = 0;

	FRotator InitialRotation;
};
